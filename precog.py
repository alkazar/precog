
import requests
import json


PRIVATE_TOKEN = ''
PROJECT_ID = ''


events = []
last_activity = {}
branches = {}

url = 'https://gitlab.com/api/v3/projects/{project_id}/merge_requests?state=closed&private_token={private_token}'
res = requests.get(url.format(project_id=PROJECT_ID, private_token=PRIVATE_TOKEN))
mrs = res.json()

for mr in mrs:
    event = {}
    event['event'] = 'create'
    event['title'] = mr['title']
    event['branch'] = mr['source_branch']
    event['datetime'] = mr['created_at']
    event['user'] = mr['author']['username']
    last_activity[event['user']] = None
    events.append(event)

    event = {}
    event['event'] = 'merge'
    event['title'] = mr['title']
    event['branch'] = mr['source_branch']
    event['datetime'] = mr['updated_at']
    try:
        event['user'] = mr['assignee']['username']
    except:
        event['user'] = None #'UNKNOWN'
    last_activity[event['user']] = None
    events.append(event)

    branches[event['branch']] = {}
    branches[event['branch']]['title'] = event['title']


events = sorted(events, key=lambda x: x['datetime'])

for event in events:
    if 'create' == event['event']:
        branches[event['branch']]['started_at'] = last_activity[event['user']]
        branches[event['branch']]['implemented_at'] = event['datetime']
        branches[event['branch']]['implemented_by'] = event['user']
    elif 'merge' == event['event']:
        branches[event['branch']]['merged_at'] = event['datetime']
        branches[event['branch']]['merged_by'] = event['datetime']
        branches[event['branch']]['merged_by'] = event['user']


    last_activity[event['user']] = event['datetime']


print(json.dumps(branches, sort_keys=True, indent=4))
